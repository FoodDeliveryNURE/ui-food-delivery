import { Component, OnInit } from '@angular/core';
import { map, Observable, take } from 'rxjs';
import { RoleEnum } from 'src/app/models/enums/Role';
import { ProductWithCountInBasket } from 'src/app/models/Order';
import { UserView } from 'src/app/models/User';
import { OrderService } from 'src/app/order-service/order-service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  products$: Observable<ProductWithCountInBasket[]>;
  userView: UserView;
  counterValue = 0;

  constructor(private orderService: OrderService) {
    this.userView = JSON.parse(sessionStorage.getItem('user') as string);
   }

  ngOnInit() {
    this.products$ = this.orderService.getProductsFromBasket(this.userView.userInfo.id);
  }

  increment(product: ProductWithCountInBasket){
       product.amountInBasket = product.amountInBasket + 1;
  }

  decrement(product: ProductWithCountInBasket) {
    if (product.amountInBasket === 1) {
      return;
    }

    product.amountInBasket = product.amountInBasket - 1;
  }

  sendOrders(productId: string, amount: number) {
    this.orderService.updateBasketOrder(productId, this.userView.userInfo.id as string, amount).subscribe();
  }

  removeProductFromBasket(productId: string) {
    this.orderService.removeProductFromBasket(this.userView.userInfo.id as string, productId).pipe(take(1)).subscribe();
  }
}
