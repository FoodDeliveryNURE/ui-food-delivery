import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/order-service/order-service';
import { UserService } from 'src/app/user-service/user-service';

@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.css']
})
export class LoginRegisterComponent implements OnInit {

  loginForm!: UntypedFormGroup;
  currentView: boolean = false;
  error: boolean = false;

  constructor(
    private userService: UserService, 
    private router: Router,
    private fb: UntypedFormBuilder,
    private orderService: OrderService
    ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.loginForm = this.fb.group({
      Email: ["", [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      Password: ["", [Validators.required]],
      Location: ["", [Validators.required]],
      Name: ["", [Validators.required]],
      EthereumAddress: ["", [Validators.required]]
    });
  }

  login() {
    const formVal = this.loginForm.value;
    this.userService.login(formVal).subscribe((res) => {
      if (res) {
        sessionStorage.setItem("user", JSON.stringify(res));
        this.router.navigate(["/main"]);
      }
    },
    (error: any) => {
      if (this.loginForm.get("Email")?.value && this.loginForm.get("Password")?.value) {
        this.error = true;
      }
    }
    );
  }

  register() {
    const formVal = this.loginForm.value;
    this.userService.register(formVal).subscribe((res) => {
      if (res) {
        sessionStorage.setItem("user", JSON.stringify(res));
        this.router.navigate(["/main"]);
      }
    }, 
    (error: any) => {
      if (this.loginForm.get("Email")?.value) {
        this.error = true;
      }
    }
    )
  }

  formView() {
    this.currentView = !this.currentView;
    this.error = false;
    this.loginForm.reset();
    this.loginForm.clearValidators();
    this.loginForm.updateValueAndValidity();
  }
}
