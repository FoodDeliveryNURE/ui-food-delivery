import { RoleEnum } from "./enums/Role";

export interface User {
    id?: string;
    name?: string;
    email?: string;
    location?: string;
    role?: RoleEnum;
    level?: number;
    balance?: number;
    ethereumAddress?: string;
}

export interface AuthViewJwt {
    jwt: string;
    expiredIn: number;
  }
  
export interface UserView {
    userInfo: User;
    authJwt: AuthViewJwt;
  }