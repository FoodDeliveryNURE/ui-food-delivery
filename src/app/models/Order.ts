export interface Product {
    id: string;
    productName?: string;
    price?: number;
    description?: string;
    imageURL?: string;
}

export interface Basket {
    id: string;
    order?: Product;
}

export interface ProductWithCountInBasket {
   id: string;
   productName?: string;
   price?: number;
   description?: string;
   imageURL?: string;
   amountInBasket: number;
}