export interface Nfts {
    id: string;
    nameToken?: string;
    userId?: string;
    txHash?: string;
    url?: string;
    ipfsUrl?: string;
    ethereumAddress?: string;
}