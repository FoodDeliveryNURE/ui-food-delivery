import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from '../../models/Order';

@Component({
  selector: 'app-order-create-dialog',
  templateUrl: './order-create-dialog.component.html',
  styleUrls: ['./order-create-dialog.component.css']
})
export class OrderCreateDialogComponent {

  product: Product;

  constructor(
    public dialogRef: MatDialogRef<OrderCreateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product,
  ) {}

  onClose(): void {
    this.dialogRef.close();
  }

  submit() {
    this.product = this.data;
    return this.product;
  }
}
