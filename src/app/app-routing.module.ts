import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './authGuard/auth-guard';
import { BasketComponent } from './basket-component/basket/basket.component';
import { LoginRegisterComponent } from './login-register/login-register/login-register.component';
import { MainPageComponent } from './main-page-component/main-page/main-page.component';
import { NftsComponentComponent } from './nfts-component/nfts-component/nfts-component.component';

const routes: Routes = [
  { path: "", component: LoginRegisterComponent },
  { path: "main", 
    component: MainPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "basket",
    component: BasketComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "nfts",
    component: NftsComponentComponent,
    canActivate: [AuthGuard]
  },
  { path: "**", redirectTo: "/"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
