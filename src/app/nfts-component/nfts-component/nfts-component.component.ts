import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Nfts } from 'src/app/models/NFts';
import { UserView } from 'src/app/models/User';
import { UserService } from 'src/app/user-service/user-service';

@Component({
  selector: 'app-nfts-component',
  templateUrl: './nfts-component.component.html',
  styleUrls: ['./nfts-component.component.css']
})
export class NftsComponentComponent implements OnInit {

  nfts$: Observable<Nfts[]>;
  userView: UserView;

  constructor(private userService: UserService) { 
    this.userView = JSON.parse(sessionStorage.getItem('user') as string);
  }

  ngOnInit() {
    this.nfts$ = this.userService.fetchUserNfts(this.userView?.userInfo?.id as string);
  }
}
