import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { RoleEnum } from "../models/enums/Role";
import { Basket, Product, ProductWithCountInBasket } from "../models/Order";

@Injectable()
export class OrderService {

    constructor(private http: HttpClient) { }

    createOrder(order: Product) {
        return this.http.post<Product>(`${environment.apiUrl}/Product/AddNewProduct`, order);
    }

    getAllProducts() {
        return this.http.get<Product[]>(`${environment.apiUrl}/Product/GetAllProducts`);
    }

    removeProduct(productId: string) {
        const options = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
            }),
            body: {
              productId
            },
          };
        return this.http.delete<any>(`${environment.apiUrl}/Product/RemoveProduct`, options);
    }

    createBasket(basketId: string) {
        return this.http.post<Basket>(`${environment.apiUrl}/Basket/AddNewBasket?basketId=${basketId}`, basketId)
    }

    addToBasket(basketId: string, productId: string) {
        return this.http.post<Product>(`${environment.apiUrl}/Basket/AddProductToBasket`, {basketId, productId});
    }

    getProductsFromBasket(basketId: string | undefined) {
        return this.http.get<ProductWithCountInBasket[]>(`${environment.apiUrl}/Basket/GetListOfProductsFromBasket?basketId=${basketId}`)
    }

    sendOrders(basketId: string | undefined) {
        return this.http.get<ProductWithCountInBasket[]>(`${environment.apiUrl}/Basket/ApproveOrder?basketId=${basketId}`);
    }

    updateBasketOrder(productId: string, basketId: string, amount: number) {
        const options = {
            productId: productId,
            basketId: basketId,
            updatedNumber: amount
        };
        return this.http.put<ProductWithCountInBasket>(`${environment.apiUrl}/Basket/UpdateAmountOfProductInBasket`, options);
    }

    getFullPriceOfBasket(basketId: string) {
        return this.http.get<number>(`${environment.apiUrl}/Basket/GetFullPriceOfBasket?basketId=${basketId}`);
    }

    getProductById(productId: string) {
        return this.http.get<Product>(`${environment.apiUrl}/Product/GetProductById?productId=${productId}`);
    }

    updateProduct(product: Product) {
        const options = {
            productId: product.id,
            productName: product.productName,
            price: product.price,
            description: product.description,
            imageURL: product.imageURL
        };

        return this.http.put<Product>(`${environment.apiUrl}/Product/UpdateProduct`, options);
    }

    removeProductFromBasket(basketId: string, productId: string) {
        const options = {
           basketId: basketId,
           productId: productId
        };

        return this.http.post<ProductWithCountInBasket>(`${environment.apiUrl}/Basket/RemoveProductFromBasket`, options);
    }
}