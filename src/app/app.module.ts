import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { HeaderComponentComponent } from './header-component/header-component/header-component.component';
import { MainPageComponent } from './main-page-component/main-page/main-page.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule} from '@angular/material/sidenav';
import { MatDividerModule} from '@angular/material/divider';
import { SideNavComponent } from './sidenav-component/side-nav/side-nav.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user-service/user-service';
import { AuthGuard } from './authGuard/auth-guard';
import { LoginRegisterComponent } from './login-register/login-register/login-register.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OrderService } from './order-service/order-service';
import { MatDialogModule } from '@angular/material/dialog';
import { OrderCreateDialogComponent } from './order-create-dialog/order-create-dialog/order-create-dialog.component';
import { BasketComponent } from './basket-component/basket/basket.component';
import { BasketSubmitDialogComponent } from './basket-submit-dialog/basket-submit-dialog/basket-submit-dialog.component';
import { OrderChangeDialogComponent } from './order-change-dialog/order-change-dialog/order-change-dialog.component';
import { UserBalanceReplenishDialogComponent } from './user-balance-replenish-dialog/user-balance-replenish-dialog/user-balance-replenish-dialog.component';
import { NftsComponentComponent } from './nfts-component/nfts-component/nfts-component.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    MainPageComponent,
    SideNavComponent,
    LoginRegisterComponent,
    OrderCreateDialogComponent,
    BasketComponent,
    BasketSubmitDialogComponent,
    OrderChangeDialogComponent,
    UserBalanceReplenishDialogComponent,
    NftsComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatButtonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatDividerModule,
    HttpClientModule,
    FlexLayoutModule,
    MatDialogModule,
    FormsModule
  ],
  providers: [UserService, AuthGuard, OrderService],
  exports: [SideNavComponent, HeaderComponentComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
