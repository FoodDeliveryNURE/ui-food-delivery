import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, NgModel } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, take } from 'rxjs';
import { UserView } from 'src/app/models/User';
import { OrderService } from 'src/app/order-service/order-service';

@Component({
  selector: 'app-basket-submit-dialog',
  templateUrl: './basket-submit-dialog.component.html',
  styleUrls: ['./basket-submit-dialog.component.css']
})
export class BasketSubmitDialogComponent implements OnInit {

  submitDialog: boolean;
  basketPrice: number;
  userView: UserView;

  constructor(
    public dialogRef: MatDialogRef<BasketSubmitDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: boolean,
    private orderService: OrderService
  ) {
    this.userView = JSON.parse(sessionStorage.getItem('user') as string);
  }

  ngOnInit(): void {
    this.orderService.getFullPriceOfBasket(this.userView.userInfo.id as string)
    .pipe(take(1))
    .subscribe((res) => {
      this.basketPrice = res;
    }); 
  }

  onClose(): void {
    this.data = false;
    this.dialogRef.close();
  }

  submit() {
    this.data = true;
    this.submitDialog = this.data;
    return this.submitDialog;
  }

  invalid() {
    const balance = this.userView?.userInfo?.balance as number;
    return this.basketPrice > balance;
  }
}
