import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { RoleEnum } from "../models/enums/Role";
import { User } from "../models/User"
import { Nfts } from "../models/NFts";

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    login(user: User) {
        return this.http.post<User>(`${environment.authApiUrl}/users/login`, user);
    }

    register(user: User) {
        return this.http.post<User>(`${environment.authApiUrl}/users/register`, user);
    }

    userOrderUpdate(userId: string, fullPrice: number) {
        const options = {
            totalAmountOfProducts: 1,
            fullPriceOfPurchase: fullPrice
        };

        return this.http.put<User>(`${environment.authApiUrl}/users/${userId}/buy`, options);
    }

    userBalanceReplanish(userId: string, money: number) {
        return this.http.put<User>(`${environment.authApiUrl}/users/${userId}/topUpWallet/${money}`, null);
    }

    fetchUserNfts(userId: string) {
        return this.http.get<Nfts[]>(`${environment.authApiUrl}/users/${userId}/nfts`);
    }
}