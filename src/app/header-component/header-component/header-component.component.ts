import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { take } from 'rxjs';
import { BasketSubmitDialogComponent } from 'src/app/basket-submit-dialog/basket-submit-dialog/basket-submit-dialog.component';
import { UserView } from 'src/app/models/User';
import { OrderService } from 'src/app/order-service/order-service';
import { UserService } from 'src/app/user-service/user-service';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {

  @Input()
  buttonActions: boolean;
  userView: UserView;
  submitDialog: boolean;

  constructor(private orderService: OrderService,
    private userService: UserService,
    private dialog: MatDialog) {}

  ngOnInit() {
    this.userView = JSON.parse(sessionStorage.getItem('user') as string);
  }

  openDialog() {
    const basketId = this.userView.userInfo.id;
    const balance = this.userView.userInfo.balance as number;

    const dialogRef = this.dialog.open(BasketSubmitDialogComponent, {
      height: '160px',
      width: '500px',
      data: {submitDialog: this.submitDialog},
    });

    dialogRef.afterClosed().subscribe(result => {
       if (result === true) {
        this.orderService.getFullPriceOfBasket(this.userView.userInfo.id as string).pipe(take(1)).subscribe((res) => {
          if (res && balance > res) {
            this.userService.userOrderUpdate(basketId as string, res).subscribe((data) => {
                if (data) {
                  sessionStorage.setItem("balance", JSON.stringify(balance - res));
                  this.orderService.sendOrders(basketId).subscribe();
                }
               });
          }
        });
       }
    });
  }
}
