import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User, UserView } from 'src/app/models/User';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { OrderCreateDialogComponent } from 'src/app/order-create-dialog/order-create-dialog/order-create-dialog.component';
import { Product } from 'src/app/models/Order';
import { OrderService } from 'src/app/order-service/order-service';
import { RoleEnum } from 'src/app/models/enums/Role';
import { UserBalanceReplenishDialogComponent } from 'src/app/user-balance-replenish-dialog/user-balance-replenish-dialog/user-balance-replenish-dialog.component';
import { UserService } from 'src/app/user-service/user-service';
import { take } from 'rxjs';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

  userView: UserView;
  product: Product;
  basketId: string;
  role = RoleEnum;
  topUpPrice: number;

  userUpdatedBalance: string;

  constructor(private router: Router,
    private dialog: MatDialog,
    private orderService: OrderService,
    private userService: UserService) { }

  ngOnInit() {
    this.userView = JSON.parse(sessionStorage.getItem('user') as string);
    this.userUpdatedBalance = JSON.parse(sessionStorage.getItem('balance') as string);
  }

  logout() {
    sessionStorage.removeItem("user");
    this.router.navigate(['/']);
  }

  openDialog() {
    const dialogRef = this.dialog.open(OrderCreateDialogComponent, {
      height: '650px',
      width: '400px',
      data: {product: this.product},
    });

    dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.orderService.createOrder(result).subscribe();
       }
    });
  }

  createBasket() {
    this.basketId = this.userView.userInfo.id as string;
    this.orderService.createBasket(this.basketId).subscribe((res) => {
    });
  }

  increaseBalance() {
    const dialogRef = this.dialog.open(UserBalanceReplenishDialogComponent, {
      height: '250px',
      width: '500px',
      data: this.topUpPrice,
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.userService.userBalanceReplanish(this.userView.userInfo.id as string, res)
          .pipe(take(1))
          .subscribe((data) => {
            sessionStorage.setItem("balance", JSON.stringify(data?.balance));
          });
      }
    });
  }
}
