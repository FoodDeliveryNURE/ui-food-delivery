import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs';
import { Product } from 'src/app/models/Order';
import { OrderService } from 'src/app/order-service/order-service';

@Component({
  selector: 'app-order-change-dialog',
  templateUrl: './order-change-dialog.component.html',
  styleUrls: ['./order-change-dialog.component.css']
})
export class OrderChangeDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<OrderChangeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.orderService.getProductById(this.data.id).pipe(take(1)).subscribe();
  }

  onClose() {
    this.dialogRef.close();
  }

  submit() {
    return this.data;
  }
}
