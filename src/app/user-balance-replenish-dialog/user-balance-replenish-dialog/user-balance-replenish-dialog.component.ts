import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-user-balance-replenish-dialog',
  templateUrl: './user-balance-replenish-dialog.component.html',
  styleUrls: ['./user-balance-replenish-dialog.component.css']
})
export class UserBalanceReplenishDialogComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<UserBalanceReplenishDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number) { }

  ngOnInit() {
  }

  onClose(): void {
    this.dialogRef.close();
  }

  submit() {
    return this.data;
  }
}
