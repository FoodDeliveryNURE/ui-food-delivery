import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { map, Observable, of, take, tap } from 'rxjs';
import { Product } from 'src/app/models/Order';
import { UserView } from 'src/app/models/User';
import { RoleEnum } from 'src/app/models/enums/Role';
import { OrderChangeDialogComponent } from 'src/app/order-change-dialog/order-change-dialog/order-change-dialog.component';
import { OrderService } from 'src/app/order-service/order-service';
import { UserService } from 'src/app/user-service/user-service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  products$: Observable<Product[]>;
  userView: UserView;
  basketId: string;
  role = RoleEnum;

  constructor(private orderService: OrderService,
    private userService: UserService,
    private dialog: MatDialog) {
    this.userView = JSON.parse(sessionStorage.getItem('user') as string);
  }

  ngOnInit(): void {
    this.products$ = this.orderService.getAllProducts();
    this.userService.fetchUserNfts(this.userView?.userInfo?.id as string).subscribe();
  }

  removeProduct(productId: string) {
    this.orderService.removeProduct(productId).subscribe({
      next: data => {
          return 'Delete successful';
      },
      error: error => {
          alert(`There was an error! ${error}`);
      }
  });
  }

  addToBasket(productId: string) {
    this.basketId = this.userView.userInfo?.id as string;
    this.orderService.addToBasket(this.basketId, productId).subscribe();
  }

  changeProduct(product: Product) {
    const dialogRef = this.dialog.open(OrderChangeDialogComponent, {
      height: '650px',
      width: '400px',
      data: product,
    });

    dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.orderService.updateProduct(result).pipe(take(1)).subscribe();
       }
    });
  }
}
