export const environment = {
    production: false,
    apiUrl: "http://localhost:5116/api",
    authApiUrl: "http://localhost:5117/api"
  };